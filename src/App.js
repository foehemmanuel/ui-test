import { CssBaseline, Grid } from "@material-ui/core";
import Dashboard from "./Components/Dashboard/Dashboard";
import Notifications from "./Components/Notifications/Notification";
import Report from "./Components/Report/Report";
import Settings from "./Components/Settings/Settings";
import Sidebar from "./Components/Sidebar/Sidebar";
import Users from "./Components/Users/Users";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  return (
    <>
      <CssBaseline />
      <Grid container style={{ width: "100%" }}>
        <Router>
          <Grid item xs={2} sm={2} md={3}>
            <Sidebar />
          </Grid>
          <Grid item xs={10} sm={10} md={9}>
            <Switch>
              <Route exact path="/">
                <Dashboard />
              </Route>
              <Route path="/users">
                <Users />
              </Route>
              <Route path="/reports">
                <Report />
              </Route>
              <Route path="/notifications">
                <Notifications />
              </Route>
              <Route path="/settings">
                <Settings />
              </Route>
            </Switch>
          </Grid>
        </Router>
      </Grid>
    </>
  );
}

export default App;
