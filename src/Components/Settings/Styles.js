import { makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => ({
  settings: {
    height: "100vh",
    padding: "1rem 1.5rem",
    backgroundColor: "gray",
  },
  header: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    // backgroundColor: "green",
  },

  title: {
    margin: "0.4rem 0",
    fontSize: "1rem",
  },

  navProfile: {
    display: "flex",
  },
}));
