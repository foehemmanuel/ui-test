import React from "react";
import { Avatar } from "@material-ui/core";
import ArrowDropDownOutlinedIcon from "@material-ui/icons/ArrowDropDownOutlined";
import useStyles from "./Styles";

const Settings = () => {
  const classes = useStyles();
  return (
    <div className={`${classes.settings}`}>
      <header className={classes.header}>
        <h1 className={classes.title}>Settings</h1>
        <div className={classes.navProfile}>
          <Avatar />
          <ArrowDropDownOutlinedIcon />
        </div>
      </header>
    </div>
  );
};

export default Settings;
