import {
  Badge,
  makeStyles,
  Menu,
  MenuItem,
  withStyles,
} from "@material-ui/core";

export default makeStyles((theme) => ({
  report: {
    padding: "1rem 1.5rem",
    height: "100vh",
    overflowY: "scroll",
    backgroundColor: "rgb(245 248 248);",
  },

  header: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },

  title: {
    margin: "0.4rem 0",
    fontSize: "1rem",
    color: "#178d9e",
  },

  navProfile: {
    width: "5rem",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "white",
    padding: "0 0.8rem",
    borderRadius: "20px",
  },

  small: {
    width: theme.spacing(4),
    height: theme.spacing(4),
  },

  //   form Style

  form: {
    display: "flex",
    alignItems: "center",
    width: "100%",
    margin: "3rem 0",
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column",
      height: "40vh ",
      padding: "0.8rem ",
    },
  },
  formItems: {
    width: "100%",
    height: "100%",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column",
      alignItems: "center",
    },
  },

  reportType: {
    marginRight: "1rem",
  },

  label: {
    marginRight: "0.8rem",
    [theme.breakpoints.down("sm")]: {
      textAlign: "center",
      marginBottom: "0.7rem",
    },
  },

  sel: {
    borderRadius: "20px",
    padding: "0.4rem 1rem",
    backgroundColor: "white",
    boxShadow: "  0.25em 0.25em  0.25em  rgba( 0, 0, 0, 0.08 )",
  },

  select: {
    width: "13rem",
    paddingRight: "1rem",
    backgroundColor: "white",
    border: "none",
    fontSize: "0.8rem",
  },
  data_input: {
    width: "9rem",
    border: "none",
    fontSize: "1rem",
    backgroundColor: "white",
  },

  dateInfo: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },

  dateFrom: {
    marginRight: "2rem",
    [theme.breakpoints.down("sm")]: {
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
    },
  },

  dateTo: {
    [theme.breakpoints.down("sm")]: {
      display: "flex",
      flexDirection: "column",
    },
  },

  first_date: {
    backgroundColor: "white",
    borderRadius: "20px",
    padding: "0.4rem 1rem",
    boxShadow: "  0.25em 0.25em  0.25em  rgba( 0, 0, 0, 0.08 )",
  },

  download: {
    display: "flex",
    justifyContent: "flex-end",
    margin: "1.5rem 0",
    cursor: "pointer",
  },
  Dload: {
    width: "2rem",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: "35%",

    border: "2px solid #178d9e",
    padding: "0.3rem",
  },

  dIcon: {
    fontSize: "1rem",
    color: "#178d9e",
  },

  //   Table Style
  table: {
    width: "100%",
    borderSpacing: "0 15px",
    borderCollapse: "separate",
  },

  table_data: {
    width: "100%",
    textAlign: " center",
    padding: "0.3rem",
    boxShadow: "  0.25em 0.25em  0.25em  rgba( 0, 0, 0, 0.08 )",
  },

  table_item: {
    backgroundColor: "white",
    // padding: "0.4rem",
  },

  table_item_F: {
    borderRadius: "10px 0 0 10px",
    backgroundColor: "white",
    // padding: "0.4rem",
  },
  table_item_L: {
    borderRadius: "0 10px 10px 0",
    backgroundColor: "white",
    // padding: "0.4rem",
    color: "#178d9e",
  },

  page: {
    display: "flex",
    justifyContent: "flex-end",
    marginTop: "1rem",
  },

  pageItem: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: "10px",
    padding: "0.1rem 0.7rem",
    backgroundColor: "white",
  },
  page1: {
    padding: "0.2rem",
    color: "#178d9e",
  },
  page2: {
    padding: "0.2rem",
  },
}));

export const StyledBadge = withStyles(() => ({
  badge: {
    backgroundColor: "white",
    color: "blue",
    width: 10,
    height: 10,
    border: "2px solid currentColor",
    borderRadius: "50%",
    "&::after": {
      position: "absolute",
      top: 0,
      left: 0,
      width: "100%",
      height: "100%",
      content: '""',
    },
  },
}))(Badge);

export const StyledMenu = withStyles({
  paper: {
    border: "1px solid #d3d4d5",
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "center",
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "center",
    }}
    {...props}
  />
));

export const StyledMenuItem = withStyles((theme) => ({
  root: {
    "&:focus": {
      backgroundColor: theme.palette.primary.main,
      "& .MuiListItemIcon-root, & .MuiListItemText-primary": {
        color: theme.palette.common.white,
      },
    },
  },
}))(MenuItem);
