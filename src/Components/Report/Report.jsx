import React from "react";
import ArrowDropDownOutlinedIcon from "@material-ui/icons/ArrowDropDownOutlined";
import { Avatar, ListItemIcon, ListItemText } from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import useStyles from "./Styles";
import { StyledBadge, StyledMenu, StyledMenuItem } from "./Styles";
import { SystemUpdateAltOutlined } from "@material-ui/icons";
import DraftsIcon from "@material-ui/icons/Drafts";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import SendIcon from "@material-ui/icons/Send";
import LongMenu from "./LongMenu";

// Report component
const Report = () => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (e) => {
    setAnchorEl(e.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const tr = [1, 2, 3, 4, 5, 6];

  const classes = useStyles();
  return (
    <div className={classes.report}>
      <header className={classes.header}>
        <h1 className={classes.title}>Report</h1>
        <div className={classes.navProfile}>
          <StyledBadge
            overlap="circular"
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "right",
            }}
            variant="dot"
          >
            <Avatar
              className={classes.small}
              alt="profile pic"
              src="img\new.png"
            />
          </StyledBadge>
          <IconButton
            aria-controls="long-menu"
            aria-haspopup="true"
            aria-haspopup="true"
            onClick={handleClick}
          >
            <ArrowDropDownOutlinedIcon />
          </IconButton>
          <StyledMenu
            id="customized-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >
            <StyledMenuItem>
              <ListItemIcon>
                <SendIcon fontSize="small" />
              </ListItemIcon>
              <ListItemText primary="Sent mail" />
            </StyledMenuItem>
            <StyledMenuItem>
              <ListItemIcon>
                <DraftsIcon fontSize="small" />
              </ListItemIcon>
              <ListItemText primary="Drafts" />
            </StyledMenuItem>
            <StyledMenuItem>
              <ListItemIcon>
                <DraftsIcon fontSize="small" />
              </ListItemIcon>
              <ListItemText primary="Profile" />
            </StyledMenuItem>
            <StyledMenuItem>
              <ListItemIcon>
                <InboxIcon fontSize="small" />
              </ListItemIcon>
              <ListItemText primary="Inbox" />
            </StyledMenuItem>
            <StyledMenuItem>
              <ListItemIcon>
                <InboxIcon fontSize="small" />
              </ListItemIcon>
              <ListItemText primary="Log out" />
            </StyledMenuItem>
          </StyledMenu>
        </div>
      </header>

      <form className={classes.form}>
        <div className={classes.formItems}>
          <div className={classes.reportType}>
            <label className={classes.label} for="report">
              Report Type
            </label>
            <span className={classes.sel}>
              <select className={classes.select} name="report_type" id="report">
                <option value="crude">Gen fuel Consumption</option>
                <option value="crude">crude</option>
                <option value="crude">crude</option>
                <option value="crude">crude</option>
              </select>
            </span>
          </div>

          <div className={classes.dateInfo}>
            <span className={classes.dateFrom}>
              <label className={classes.label} for="datefrom">
                Date from
              </label>
              <span className={classes.first_date}>
                <input
                  className={classes.data_input}
                  type="date"
                  name="datefrom"
                  id="date"
                />
              </span>
            </span>

            <span className={classes.dateTo}>
              <label className={classes.label} for="dateto">
                Date to
              </label>
              <span className={classes.first_date}>
                <input
                  className={classes.data_input}
                  type="date"
                  name="dateto"
                  id="dateto"
                />
              </span>
            </span>
          </div>
        </div>
      </form>

      <div className={classes.download}>
        <span className={classes.Dload}>
          {" "}
          <SystemUpdateAltOutlined className={classes.dIcon} />
        </span>
      </div>

      {/* Table data */}
      <table className={classes.table}>
        <thead>
          <tr class={classes.table_head}>
            <th>Source</th>
            <th>Date</th>
            <th>Hour(s) run</th>
            <th>Fuel Consumed(L)</th>
            <th>Fuel/Hour(L/H)</th>
            <th>Options</th>
          </tr>
        </thead>
        <tbody>
          {tr.map((index) => (
            <tr key={index} class={classes.table_data}>
              <td className={classes.table_item_F}>Generator</td>
              <td className={classes.table_item}>04/08/2021</td>
              <td className={classes.table_item}>2.45</td>
              <td className={classes.table_item}>1.78</td>
              <td className={classes.table_item}>3.4</td>
              <td className={classes.table_item_L}>
                <LongMenu />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <div className={classes.page}>
        <div className={classes.pageItem}>
          <span className={classes.page1}>1</span>|
          <span className={classes.page2}>5</span>
        </div>
      </div>
    </div>
  );
};

export default Report;
