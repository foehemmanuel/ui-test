import { Avatar, Badge, Typography } from "@material-ui/core";
import React from "react";
import {
  Dashboard,
  GroupAddOutlined,
  NotificationImportant,
  ReportOutlined,
  Settings,
} from "@material-ui/icons";
import { Link } from "react-router-dom";
import { useLocation } from "react-router-dom";
import "./style.css";
import useStyles from "./Styles";
import { StyledBadge } from "./Styles";

const Sidebar = () => {
  const location = useLocation();
  const { pathname } = location;
  const splitLocation = pathname.split("/");

  const classes = useStyles();

  return (
    <div className={classes.sidebar}>
      <div className={classes.sidebarWrapper}>
        <h1 className={classes.logoName}>Genmon</h1>
        <div className={classes.sibebarMenu}>
          <div className={classes.profile}>
            <div className={classes.root}>
              <StyledBadge
                overlap="circular"
                anchorOrigin={{
                  vertical: "bottom",
                  horizontal: "right",
                }}
                variant="dot"
              >
                <Avatar
                  alt="profile pic"
                  src="img\new.png"
                  className={classes.large}
                />
              </StyledBadge>
            </div>
            <span className={classes.proInfo}>
              <h4>Emmanuel foeh</h4>
              <Typography>Administrator</Typography>
            </span>
          </div>

          <div
            className={`${classes.menuItem} ${
              splitLocation[1] === "" ? "menuItemActive" : ""
            } `}
          >
            <Link to="/">
              <Dashboard className={classes.icon} />

              <Typography>Dashboard</Typography>
            </Link>
          </div>
          <div
            className={`${classes.menuItem}  ${
              splitLocation[1] === "reports" ? "menuItemActive" : ""
            }  `}
          >
            <Link to="/reports">
              <ReportOutlined className={classes.icon} />
              <Typography>Report</Typography>
            </Link>
          </div>
          <div
            className={`${classes.menuItem} ${
              splitLocation[1] === "users" ? "menuItemActive" : ""
            }`}
          >
            <Link to="/users">
              <GroupAddOutlined className={classes.icon} />
              <Typography>Users</Typography>
            </Link>
          </div>
          <div
            className={`${classes.menuItem}  ${
              splitLocation[1] === "notifications" ? "menuItemActive" : ""
            }`}
          >
            <Link to="/notifications">
              <Badge className={classes.icon} badgeContent={4} color="error">
                <NotificationImportant />
              </Badge>
              <Typography>Notifications</Typography>
            </Link>
          </div>
          <div
            className={`${classes.menuItem}  ${
              splitLocation[1] === "settings" ? "menuItemActive" : ""
            }`}
          >
            <Link to="/settings">
              <Settings className={classes.icon} />
              <Typography>Settings</Typography>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Sidebar;
