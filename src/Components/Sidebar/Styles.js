import { Badge, withStyles } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => ({
  sidebar: {
    height: "100vh",
    backgroundColor: "White",
    position: "sticky",
    top: "0",
  },

  sidebarWrapper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },

  logoName: {
    width: "9rem",
    color: " #178d9e",
    textAlign: "center",
    border: "4px solid #178d9e",
    borderRadius: "20px",
  },
  sibebarMenu: {
    width: "85%",
    marginTop: "2rem",
  },

  profile: {
    display: "flex",
    alignItems: "center",
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column",
      justifyContent: "flex-start",
    },
  },
  large: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },

  root: {
    display: "flex",
    "& > *": {
      margin: theme.spacing(1),
    },
  },

  proInfo: {
    marginLeft: "1.5rem",
    "& h4": {
      margin: "0 0",
    },
    "& p": { color: "grey" },
  },

  menuItem: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    padding: "0.4rem",
    marginTop: "2rem",
    "& a": {
      display: "flex",
      width: "100%",
      height: "100%",
      textDecoration: "none",
      color: "black",
    },
    "& p": {
      [theme.breakpoints.down("sm")]: {
        display: "none",
      },
    },
    "&:hover": {
      backgroundColor: " rgb(226, 236, 236)",
    },
  },

  icon: {
    marginRight: "1.5rem",
    marginLeft: "0.7rem",
  },
}));

export const StyledBadge = withStyles(() => ({
  badge: {
    backgroundColor: "white",
    color: "blue",
    width: 10,
    height: 10,
    border: "2px solid currentColor",
    borderRadius: "50%",
    "&::after": {
      position: "absolute",
      top: 0,
      left: 0,
      width: "100%",
      height: "100%",
      content: '""',
    },
  },
}))(Badge);
