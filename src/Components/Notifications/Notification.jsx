import { Avatar } from "@material-ui/core";
import ArrowDropDownOutlinedIcon from "@material-ui/icons/ArrowDropDownOutlined";
import React from "react";
import useStyles from "./Styles";

const Notifications = () => {
  const classes = useStyles();
  return (
    <div className={`${classes.notifications}`}>
      <header className={classes.header}>
        <h1 className={classes.title}>Notifications</h1>
        <div className={classes.navProfile}>
          <Avatar />
          <ArrowDropDownOutlinedIcon />
        </div>
      </header>
    </div>
  );
};

export default Notifications;
